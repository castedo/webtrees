# webtrees

This git repository contains RPM package sources for the webtrees RPM package at
<https://copr.fedorainfracloud.org/coprs/castedo/webtrees/>.

## Installation Instructions

```
dnf copr enable castedo/webtrees 
dnf install policycoreutils-python-utils
dnf install webtrees
```

This webtrees RPM package only installs files originating from the [webtrees](https://webtrees.net) project.

For a functioning webtrees website, additional steps are required:

* Create `/var/lib/webtrees/config.ini.php` with settings.
* Create HTTP server (e.g., Apache) configuration files.
* Create a webtrees database (e.g., with MariaDB).
* Set up an SMTP email server (e.g., Postfix).
* Set SELinux booleans (e.g., `httpd_can_network_connect`, `can_sendmail`, `httpd_can_network_connect_db`, `httpd_read_user_content`).


## History

These sources are derived from the following openSUSE Build Service projects:

* <https://build.opensuse.org/package/show/home:castedo/webtrees1>
* <https://build.opensuse.org/package/show/server:php:applications/webtrees>
* <https://build.opensuse.org/package/show/home:ecsos:server/webtrees>

## Packager Instructions

To manually build an updated RPM package, edit `rpm/webtrees.spec` and

```
cd rpm
./download_and_recompress.sh
```

Inside the `bldr` container, build SRPM:
```
dnf install httpd
cd rpm
rpmbuild --define "_topdir `pwd`" -bs webtrees.spec
```

At <https://copr.fedorainfracloud.org/coprs/castedo/webtrees/>
under Builds, upload `SRPMS/webtrees-1.....src.rpm` in the container.
