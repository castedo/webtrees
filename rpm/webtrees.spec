#
# spec file for package webtrees
#
# Copyright (c) 2023 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.
#

%define         webtreesdir       %{phplibdir}/%{name}
%define         httpconfdir        %{_sysconfdir}/httpd
%define         httpduser         apache
%define         httpdgroup        apache
%define         phplibdir         %{_datadir}/php

Name:           webtrees
Version:        2.1.20
Release:        2%{?dist}
Summary:        Online collaborative genealogy application
License:        GPL-3.0-only
Group:          Productivity/Networking/Web/Frontends
URL:            https://www.webtrees.net
# original source is located here
# Source0:        https://github.com/fisharebest/webtrees/releases/download/%%{version}/webtrees-%%{version}.zip
# Use the download_and_recompress.sh script to download the sources matching the spec file's version.
Source0:        %{name}-%{version}.tar.xz
BuildRequires:  httpd
BuildRequires:  xz
Requires:       httpd >= 2.4
Requires:       mariadb >= 10.2
Requires:       php >= 7.4
Requires:       php-common
Requires:       php-gd
Requires:       php-intl
Requires:       php-mbstring
Requires:       php-mysqlnd
Requires:       php-pdo
Requires:       php-xml
Requires:       php-pecl-zip
BuildArch:      noarch

%description
View and edit family history on your website, with collaboration, privacy, and
photo features. Webtrees works from standard GEDCOM files, and is therefore
compatible with every major desktop application; and it aims to be efficient
and effective by using the right combination of third-party tools, design
techniques and open standards.

%prep
%setup -q
# remove .htaccess files
find . -name \.htaccess -print | xargs rm -f

%install
install -m 755 -d %{buildroot}%{webtreesdir}
cp -r . %{buildroot}%{webtreesdir}

# move data directory to /var/lib/ and set symlink
install -m 755 -d %{buildroot}%{_localstatedir}/lib/
mv %{buildroot}%{webtreesdir}/data %{buildroot}%{_localstatedir}/lib/%{name}
ln -s %{_localstatedir}/lib/%{name} %{buildroot}%{webtreesdir}/data

# install (empty) configuration-file
touch %{buildroot}%{_localstatedir}/lib/%{name}/config.ini.php

%post
if [ -x %{_sbindir}/semanage ] ; then
	# check is SELinux is installed and update settings
	semanage fcontext -a -t httpd_var_lib_t "%{_localstatedir}/lib/%{name}(/.*)?"
fi

%files
%defattr(644,root,root,755)

%dir %{webtreesdir}
%{webtreesdir}/*

# configuration and data repository
%dir %attr(750, %{httpduser}, %{httpdgroup}) %{_localstatedir}/lib/%{name}
%{_localstatedir}/lib/%{name}/index.php
%config(noreplace) %{_localstatedir}/lib/%{name}/config.ini.php

%changelog
* Sun Aug 25 2024 Castedo Ellerman <castedo@castedo.com>
- Remove Apache configuration file
- Remove post-install move of old install locations
* Sat Aug 24 2024 Castedo Ellerman <castedo@castedo.com>
- Version 2.1.20
- Build for CentOS Stream 9
* Tue Apr 30 2024 Castedo Ellerman <castedo@castedo.com>
- Version 1.7.20
- Build for RHEL8
* Mon Dec 27 2021 Castedo Ellerman <castedo@castedo.com>
- Version 1.7.19
- Build for RHEL8
* Sat Dec  7 2019 Arjen de Korte <suse+build@de-korte.org>
- Change BuildRequires to PHP independent packages
* Thu Apr  4 2019 Johannes Weberhofer <jweberhofer@weberhofer.at>
- Version 1.7.13
  * compatibility fixes for PHP 7.3
- Build for PHP7 on Leap 15+
* Sat Sep 15 2018 jweberhofer@weberhofer.at
- Cleaned up spec file
* Sat Sep 15 2018 jweberhofer@weberhofer.at
- Updated download locations
- Version 1.7.11
  * adds compatibility for MySQL 8.0, and PHP 7.1 and 7.2
  * updates to language files
  * minor security fixes
* Wed Jan 11 2017 jweberhofer@weberhofer.at
- Version 1.7.9
  * Bug fixes and updated translations
  * New German census templates
  * Improvements to the relationship calculator. You can now search for
    relationships via direct ancestors only, which is much faster on
    large/complex trees
* Sun Oct  2 2016 jweberhofer@weberhofer.at
- Version 1.7.8
  * bug fixes and updated translations
* Fri Jun 17 2016 jweberhofer@weberhofer.at
- Version 1.7.5:
  * Translations, bug fixes, and it restores compatibility with PHP 5.3.2 - 5.3.8
  * fixed a number of problems with the Google Maps module
  * New Albanian translation
  * charts have been integrated into the module system, so that you can now disable
    those that you don't use
* Sun Mar 20 2016 jweberhofer@weberhofer.at
- Version 1.7.4: bug-fixes, updated translations and more census definitions.
- New languages: ln and sw
* Wed Jan 20 2016 jweberhofer@weberhofer.at
- Version 1.7.3: a number of bug-fixes and an update to the census
  assistant tool
* Sun Sep  6 2015 jweberhofer@weberhofer.at
- Version 1.7.2: bug-fixes and updated translations
* Tue Jul 14 2015 web@vie.weberhofer.at
- Version 1.7.1: This release addresses issues realated to servers running
  PHP5.3
* Mon Jul 13 2015 jweberhofer@weberhofer.at
- Version 1.7.0: major rewrite and modernisation of backend code
- Languages:  webtrees now has support for 60 languages -- a  new
  administration option allows for easier management.  There is also a new
  online translation server (translate.webtrees.net) which makes it even easier
  to contribute to translations.
- Mobile phone support:  If you use Google webmaster tools, you may have
  received warnings about mobile support. We've made some changes to improve
  this, and have completely rewriten the administration area (now called "Control
  panel") to use a mobile-friendly layout.
- Administration:  There are a few additions to the administration functions; a
  renumbering and merging family trees and a function to help you find
  duplicate records after a merge.  webtrees checks for updates every 24 hours,
  so next time you log in to your site, you should be prompted to upgrade. An
  update to the robots.txt file has been made.
- Performance:  There are a few performance enhancements. For example, we have
  reduced the number of database calls by fetching all family members in one
  operation.
- Themes:  The theme system has been completely redesigned. Previously, if you
  wanted to make just a small modification, you needed to create and update an
  entire theme. Now, you only need to code the differences from an existing
  theme. So, if you simply want to change the header or some menus, that's all
  you need to do. Updates to the core code should be much less likely to require
  changes to the themes.
  For a complete descriptions of this release see:
  https://www.webtrees.net/index.php/en/forum/2-open-discussion/30354-webtrees-1-7-0-released#53470
- openSUSE: Use HTML Purifier's Pear package.
* Sat Apr 11 2015 cstedo@castedo.com
- Simplifed apache's config file
* Mon Mar 30 2015 jweberhofer@weberhofer.at
- Fedora based distributions must require http but not httpd2
* Mon Mar 30 2015 jweberhofer@weberhofer.at
- media and config file is now always located in /var/lib/webtrees.
  old placements in /srv/www/webtrees will be automatically moved.
- Do not longer include /etc/apache2/conf.d in directory list
- Improve configuration to support apache 2.2 and apache 2.4
  allow/deny syntax
* Fri Mar 27 2015 aj@ajaissle.de
- Build requires xz instead of unzip
* Thu Mar 26 2015 jweberhofer@weberhofer.at
- Moved the program-sources to /usr/share/webtrees and the
  data to /(var/lib|/srv/www)/webtrees. This should allow
  installation on openSUSE and Fedora.
- Package source in tar.xz format, don't use original zip file.
  Improved the download_and_recompress.sh script to download
  the sources matching the spec file's version.
- Added some PHP security constaints to the apache configuration.
- Added SELinux configuration for CentOS installations.
- Zend dependencies have been temporarily disabled. Must be reviewed.
- Cleaned up spec.
* Mon Feb  9 2015 castedo@castedo.com
- For CentOS 7 (SELinux), install to separate /var/www/webtrees
  for read-only content and /var/lib/webtrees for read-write data
- On non-SLES (CentOS 7) remove php-ZendFramework requires which is
  no longer in the distro (php-ZendFramework2 is distributed now)
- On non-SLES use original webtrees zip which contains small subset
  of ZendFramework version 1.
* Mon Dec 22 2014 jweberhofer@weberhofer.at
- Upgrade to version 1.6.2: bug fixes
- Recompress zipped source to a tar.bz2 files not containing the
  Zend library seperately installed by RPM tool.
- Improved and cleaned up apache configuration
* Tue Nov 18 2014 jweberhofer@weberhofer.at
- Upgrade to version 1.6.1: bug fixes
- moved several .htaccess rules into apache configuration
* Wed Nov  5 2014 jweberhofer@weberhofer.at
- Upgrade to version 1.6.0
  * bug fixes
  * translation updates
  * code optimizations
  * performance improvements
  * new feature: merge entire trees
* Sat May  3 2014 jweberhofer@weberhofer.at
- Upgrade to version 1.5.3
* Tue Feb 11 2014 jweberhofer@weberhofer.at
- Upgrade to version 1.5.2
  * Fixes for 30 reported bugs
  * New admin tool to update place names in bulk
* Fri Jan 10 2014 jweberhofer@weberhofer.at
- Use launchpad release instead of github release
- Include all supported languages
- Fix FSF Address
* Sat Jan  4 2014 jweberhofer@weberhofer.at
- Removed Zend framework and add requiremend to Zend packages
- Fix path for configuration files
* Thu Dec 12 2013 castedo@castedo.com
- Use different dirs and user on non-SUSE distros (e.g. Fedora).
* Tue Dec 10 2013 castedo@castedo.com
- Install HTTP configuration webtrees.conf into /etc/apache2/conf.d
* Sun Dec  8 2013 castedo@castedo.com
- 1.5.1 released
* Sat Nov  9 2013 castedo@castedo.com
- New 1.5.0 release.
