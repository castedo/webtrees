#!/bin/sh

# Package source in tar.xz format instead of original zip file.
# Use the download_and_recompress.sh script to download the sources
# matching the spec file's version.
VERSION=`grep "^Version:" webtrees.spec | sed -e 's/Version:[ \t]*//'`
URL="https://github.com/fisharebest/webtrees/releases/download/$VERSION/webtrees-$VERSION.zip"
SOURCE0="SOURCES/webtrees-$VERSION.tar.xz"

if [ -z "$VERSION" ]; then
        echo "Can't find Version definition in webtrees.spec"
        exit 1
fi

if [ -e "$SOURCE0" ] ; then
	echo "$SOURCE0 already exsists!"
	exit 0
fi
if [ ! -e "webtrees-$VERSION.zip" ] ; then
	wget "$URL"
	if [ $? -ne 0 ] ; then
		echo "Error downloading $URL"
		exit 1
	fi
fi

if [ ! -e "webtrees-$VERSION.zip" ] ; then
	echo "Could not download $URL"
	exit 2
fi

if [ -d "webtrees/" ] ; then
	rm -rf "webtrees/"
fi
if [ -d "webtrees-$VERSION/" ] ; then
	rm -rf "webtrees-$VERSION/"
fi

unzip -q "webtrees-$VERSION.zip"

mv "webtrees/" "webtrees-$VERSION/"

tar -cJf "$SOURCE0" "webtrees-$VERSION/"

rm -rf "webtrees-$VERSION/" "webtrees-$VERSION.zip"

